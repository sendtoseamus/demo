import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import GroupItemsScreen from '../screens/GroupItems';
import GroupsScreen from '../screens/GroupsScreen';
import ItemDetailsScreen from '../screens/ItemDetails';
import GroupDetailsScreen from '../screens/GroupDetails';

const GroupsNavigator = createStackNavigator({
    Groups: GroupsScreen,
    Items: GroupItemsScreen,
    Details: ItemDetailsScreen,
    GroupDetails: GroupDetailsScreen
});

export default createAppContainer(GroupsNavigator);