import firebase from 'firebase';
import Group from '../models/Group';

export async function loadUsersAsync() {
    console.log('\n--------------\nloading users snapshot');

    const db = firebase.firestore();
    db.collection('users')
        .onSnapshot(function (snapshot) {
            if (snapshot.empty) {
                console.log('users empty');
            } else {
                console.log(snapshot.docs.length + " users in snapshot\n");
            }
        });

    let groups = [];
    const snapshot = await db.collection('users').get();

    snapshot.forEach(doc => {
        console.log(doc.id, '=>', doc.data());

        const name = doc.get('name');
        const group = new Group(doc.id, name);
        groups = groups.concat(group);
    });

    console.log(groups.length + " groups in array\n");
    return groups
}

export async function loadUsers() {
    const db = firebase.firestore();
    const snapshot = await db.collection('users').get();
    let groups = [];

    snapshot.forEach(doc => {
        console.log(doc.id, '=>', doc.data());
        const name = doc.get('name');
        const group = new Group(doc.id, name);
        groups = groups.concat(group);
    });
    return groups;
};

export default function startFirebase() {

    if (firebase.apps.length === 0) {

        const firebaseConfig = {
            apiKey: "AIzaSyBwnZue9yRFL2qxmydtFvLlorfidQxNltU",
            authDomain: "app-a82db.firebaseapp.com",
            projectId: "app-a82db",
            storageBucket: "app-a82db.appspot.com",
            messagingSenderId: "93523390586",
            appId: "1:93523390586:web:188c31ea031409c92c8bc3",
            measurementId: "G-T1G2JS1SBK"
        };

        firebase.initializeApp(firebaseConfig);

        console.log('firebase initialised');
    }
};