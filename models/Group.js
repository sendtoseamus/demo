
class Group {
    constructor(
        id,
        name,
        title,
        imageUrl,
        discussionCount,
        listingsCount,
        usersCount
    ) {
        this.id = id
        this.name = name
        this.title = title
        this.imageUrl = imageUrl
        this.discussionCount = discussionCount
        this.listingsCount = listingsCount
        this.usersCount = usersCount
    }
}

export default Group;