class Item {
    constructor(
        id,
        name,
        brand,
        groupId,
        imageUrl,
        owner,
        size
    ) {
        this.id = id
        this.name = name
        this.brand = brand
        this.groupId = groupId
        this.imageUrl = imageUrl
        this.owner = owner
        this.size = size
    }
}

export default Item;