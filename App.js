import React from 'react';
import { Provider } from 'react-redux'
import AppNavigator from './navigation/AppNavigator';
import store from './store/Store';

export default function App() {
  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  );
}
  // return (
  //   <View style={styles.container}>
  //     <Text>Open up App.js to start working on your firebase app!</Text>
  //     <StatusBar style="auto" />
  //   </View>
  // );
//}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
