import React from 'react';
import { StyleSheet, FlatList, SafeAreaView, View, Button } from 'react-native';
import { useSelector } from 'react-redux';
import GroupGridItem from '../components/GroupGridItem';
import startFirebase from '../firebase/FirApp';
import { loadGroups } from '../store/actions/Groups';

const GroupsScreen = props => {
    startFirebase();
    loadGroups();

    const selectItemHandler = (itemData) => {
        props.navigation.navigate({
            routeName: 'Items',
            params: {
                group: itemData.item,
                groupName: itemData.item.name
            }
        });
    };

    const addNewGroupHandler = () => {
        props.navigation.navigate({
            routeName: 'GroupDetails'
        });
    };

    const description = (item) => {
        return item.listingsCount + ' listings - ' +
            item.discussionCount + ' discussions | United States'
    };

    const groups = useSelector(state => state.groups.groups);
    console.log(groups + ' Groups in state');

    return (
        <SafeAreaView>
            <View style={styles.buttonFrame}>
                <Button
                    title={'Add new Group'}
                    onPress={() => {
                        addNewGroupHandler();
                    }}
                >
                </Button>
            </View>
            <FlatList
                style={styles.list}
                data={groups}
                keyExtractor={item => item.id}
                renderItem={itemData => (
                    <GroupGridItem
                        title={itemData.item.title}
                        name={itemData.item.name}
                        description={description(itemData.item)}
                        users={itemData.item.usersCount + ' members'}
                        image={itemData.item.imageUrl}
                        onSelect={() => {
                            selectItemHandler(itemData);
                        }}
                    ></GroupGridItem >
                )}
            />
        </SafeAreaView >
    );
};

GroupsScreen.navigationOptions = {
    headerTitle: 'Explore Groups',
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    list: {
        marginVertical: 10
    },
    buttonFrame: {
        height: 40,
        margin: 8,
        width: '50%',
        backgroundColor: '#efefef',
        borderColor: '#828282',
        borderWidth: 2,
        borderRadius: 12,
        alignSelf: 'center'
    }
});

export default GroupsScreen;



