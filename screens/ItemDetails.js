import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { createNewItem, loadItems } from '../store/actions/Items';

const ItemDetailsScreen = props => {
    const groupId = props.navigation.getParam('groupId');
    createNewItem(groupId);
    loadItems(groupId);

    return (
        <View style={styles.screen}>
            <Text style={styles.text}>New item created successfully.</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 20
    }
});

export default ItemDetailsScreen;
