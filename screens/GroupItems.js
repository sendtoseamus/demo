import React, { useState } from 'react';
import { View, StyleSheet, SafeAreaView } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import GroupHeaderView from '../components/GroupHeaderView';
import ItemGridItem from '../components/ItemGridItem';
import { loadItems } from '../store/actions/Items';

const GroupItemsScreen = props => {

    const group = props.navigation.getParam('group');
    loadItems(group.id);

    const selectItemHandler = (groupId) => {
        props.navigation.navigate({
            routeName: 'Details',
            params: {
                groupId: groupId
            }
        });
    };

    useState()
    const items = useSelector(state => state.items.items);
    console.log(items + ' Items in state');

    return (
        <SafeAreaView>
            <GroupHeaderView
                title={group.name}
                description={'PRIVATE GROUP - ' + group.usersCount + ' Members'}
                image={group.imageUrl}
                onPress={() => {
                    selectItemHandler(group.id);
                }}
            ></GroupHeaderView>
            <FlatList
                data={items}
                keyExtractor={item => item.id}
                renderItem={itemData => (
                    <ItemGridItem
                        title={itemData.item.name}
                        description={itemData.item.brand + ' - ' + itemData.item.size}
                        owner={itemData.item.owner}
                        image={itemData.item.imageUrl}
                    ></ItemGridItem>
                )}
                numColumns={2}
                ListFooterComponent={<View style={styles.foot}></View>}
            />
        </SafeAreaView>
    );
};

GroupItemsScreen.navigationOptions = navigationData => {
    const groupName = navigationData.navigation.getParam('groupName');
    return {
        headerTitle: groupName
    };
};

const styles = StyleSheet.create({
    foot: {
        height: 240
    }
});

export default GroupItemsScreen;