import React from 'react';
import { View, Text, Image, StyleSheet, Button } from 'react-native';

const GroupHeaderView = props => {
    return (
        <View style={styles.header}>
            <View style={styles.empty}>
                <Image style={styles.image} source={{ uri: props.image }} />
            </View>
            <View style={styles.card}>
                <Text style={styles.title}>{props.title}</Text>
                <Text style={styles.description}>{props.description}</Text>
            </View>
            <View style={styles.foot}></View>
            <View style={styles.buttonFrame}>
                <Button
                    title={'Add new Item'}
                    onPress={props.onPress}
                >
                </Button>
            </View>
        </View >
    )
};

const styles = StyleSheet.create({
    header: {
        height: 280
    },
    empty: {
        height: 200
    },
    image: {
        flex: 1
    },
    card: {
        width: '80%',
        alignSelf: 'center',
        borderRadius: 12,
        height: 80,
        margin: -60,
        backgroundColor: '#efefef',
        borderColor: '#dfdfdf',
        borderWidth: 1,
        shadowColor: '#929292',
        shadowOffset: { width: 4, height: 4 },
        shadowOpacity: 1.0
    },
    title: {
        color: '#050505',
        textAlign: 'center',
        margin: 8,
        fontSize: 20,
        fontWeight: "bold"
    },
    description: {
        color: '#050505',
        textAlign: 'center',
        marginHorizontal: 4,
        fontSize: 16,
        fontWeight: "bold"
    },
    foot: {
        height: 80
    },
    buttonFrame: {
        height: 40,
        margin: -4,
        width: '50%',
        backgroundColor: '#efefef',
        borderColor: '#828282',
        borderWidth: 2,
        borderRadius: 12,
        alignSelf: 'center'
    }
});

export default GroupHeaderView;