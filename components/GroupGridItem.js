import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

const GroupGridItem = props => {
    return (
        <View>
            <View style={styles.titleView}>
                <Text style={styles.title}>{props.title}</Text>
            </View>
            <TouchableOpacity style={styles.gridItem}
                onPress={props.onSelect}
            >
                <Image
                    style={styles.image}
                    source={{ uri: props.image }}
                />
                <View style={styles.stack}>
                    <Text style={styles.title}>{props.name}</Text>
                    <Text style={styles.text}>{props.description}</Text>
                    <Text style={styles.text}>{props.users}</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    gridItem: {
        flexDirection: 'row',
        flex: 1,
        borderWidth: 1,
        borderRadius: 12,
        backgroundColor: '#f9f9f9',
        borderColor: '#aaaaaa',
        shadowColor: '#c6c6c6',
        shadowOffset: { width: 4, height: 4 },
        shadowOpacity: 1.0,
        margin: 16,
        height: 140
    },
    image: {
        aspectRatio: 1 / 1,
        borderRadius: 12,
    },
    stack: {
        flexDirection: 'column',
        flexGrow: 1,
        width: '60%',
        marginHorizontal: 4,
        padding: 8
    },
    text: {
        flex: 3,
        flexDirection: 'row',
        flexWrap: 'wrap',
        color: '#828282',
        fontSize: 16
    },
    titleView: {
        marginHorizontal: 16
    },
    title: {
        color: '#333333',
        fontSize: 18,
        fontWeight: "bold",
        flex: 2
    }
});

export default GroupGridItem;
