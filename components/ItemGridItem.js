import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const ItemGridItem = props => {
    return (
        <View style={styles.gridItem}>
            <Image
                style={styles.image}
                source={{ uri: props.image }}
            />
            <View style={styles.stack}>
                <Text style={styles.text}>{props.title}</Text>
                <Text style={styles.description}>{props.description}</Text>
                <Text style={styles.owner}>{props.owner}</Text>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    gridItem: {
        flex: 1,
        margin: 15,
        height: 320
    },
    image: {
        borderRadius: 12,
        flex: 3
    },
    stack: {
        flex: 1,
        marginVertical: 12
    },
    text: {
        flex: 1,
        color: '#050505',
        marginHorizontal: 4,
        fontSize: 18
    },
    description: {
        flex: 1,
        color: '#727272',
        marginHorizontal: 4,
        fontSize: 16
    },
    owner: {
        flex: 1,
        color: '#969696',
        marginHorizontal: 4,
        fontSize: 14
    }
});

export default ItemGridItem;