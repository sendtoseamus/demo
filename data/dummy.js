
const IMAGES = [
    'https://resize-ec.thefancy.com/resize/crop/620/thefancy/commerce/original/20161101/930887d2ea4e488c82710ade71d13999.jpg',
    'https://resize-ec.thefancy.com/resize/crop/620/thefancy/commerce/original/20191213/a2b7105850524a15b2d7709f10b9fe91.jpg',
    'https://resize-ec.thefancy.com/resize/crop/620/thefancy/commerce/original/20200713/13d2e4c492744500a504245b949677d7.jpg',
    'https://resize-ec.thefancy.com/resize/crop/620/thefancy/commerce/original/20201105/1c5f47bc011b4bed8e985de49ac08897.jpg',
    'https://resize-ec.thefancy.com/resize/crop/620/thefancy/commerce/original/20131212/39159e68395d453e8e018395dde826cd.jpg',
    'https://resize-ec.thefancy.com/resize/crop/620/thefancy/commerce/original/20131212/39159e68395d453e8e018395dde826cd.jpg',
    'https://resize-ec.thefancy.com/resize/crop/320/thingd/home/135/Untitled%20design%20(10)_dc6b05fe641b.png',
    'https://resize-ec.thefancy.com/resize/crop/640/thingd/home/135/Untitled%20design%20(16)_325a81e7296b.png',
    'https://resize-ec.thefancy.com/resize/crop/320/thingd/home/135/Untitled%20design%20(13)_ddc46a79899a.png',
    'https://resize-ec.thefancy.com/resize/crop/620/thefancy/commerce/original/20210204/f8a7c8444f044dfe8a5a530f65e95288.jpg',
    'https://resize-ec.thefancy.com/resize/crop/620/thefancy/commerce/original/20201105/1c5f47bc011b4bed8e985de49ac08897.jpg'
];

const GROUP_NAMES = [
    'Urban boys club',
    'Lion likely protect',
    'Loyalty player',
    'Enter real',
    'Researcher drill',
    'Train normal',
    'Station divorce',
    'Collar rage dash'
];

const GROUP_TITLE = [
    'EXPLORE YOUR CITY',
    'Drive Me Nuts',
    'Flea Market',
    'Tug of War',
    'Sharpest Tool',
    'Cry Over Spilt Milk',
    'A Leg Up',
    'Burst Your Bubble'
];

const ITEM_NAMES = [
    'Large Frame',
    'Color Spots',
    'Ash Stopper',
    'Mens Coat',
    'Summer hat',
    'Something blue',
    'Older item',
    'Cool stuff'
];

const BRANDS = [
    'Abc Outside',
    'Old wall',
    'Home bits',
    'Abc Outside',
    'Asos',
    'BK',
    'Prince Barrett',
    'Hugo Hanson',
    'Madelyn Rivera',
    'Kenya Tapia',
    'Elliott Benton',
    'Rey Golden'
];

const OWNERS = [
    'Tim',
    'Oscar',
    'Joe',
    'Seamus',
    'Alex',
    'Tanya',
    'Carley',
    'Kendall Gamble',
    'Mason Mccullough',
    'Trisha Knox',
    'Josefina Clark',
    'Ursula Vasquez',
    'Lina Haley',
    'Blanche Webb'
];

const SIZES = [
    'XS',
    'S',
    'M',
    'L',
    'XL'
];

export const randomValue = (max) => {
    return Math.floor((Math.random() * max));
};

export const randomImage = () => {
    const i = randomValue(IMAGES.length);
    return IMAGES[i];
};

export const randomGroupName = () => {
    const i = randomValue(GROUP_NAMES.length);
    return GROUP_NAMES[i];
};

export const randomGroupTitle = () => {
    const i = randomValue(GROUP_TITLE.length);
    return GROUP_TITLE[i];
};

export const randomItemName = () => {
    const i = randomValue(ITEM_NAMES.length);
    return ITEM_NAMES[i];
};

export const randomBrand = () => {
    const i = randomValue(BRANDS.length);
    return BRANDS[i];
};

export const randomOwner = () => {
    const i = randomValue(OWNERS.length);
    return OWNERS[i];
};

export const randomSize = () => {
    const i = randomValue(SIZES.length);
    return SIZES[i];
};
