import { CREATE_ITEM, LOAD_ITEMS } from "../actions/Items";

const initialState = {
    items: []
};

const itemsReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_ITEMS:
            return {
                ...state,
                items: state.items = action.items
            }
        case CREATE_ITEM:
            return state;
        default:
            return state;
    };
    return state
};

export default itemsReducer;