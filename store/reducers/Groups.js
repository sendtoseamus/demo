import { CREATE_GROUP, loadGroups, LOAD_GROUPS } from "../actions/Groups";

const initialState = {
    groups: []
};

const groupsReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_GROUPS:
            return {
                ...state,
                groups: state.groups = action.groups
            }
        case CREATE_GROUP:
            return state;
        default:
            return state;
    };
    return state
};

export default groupsReducer;