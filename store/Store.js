import { createStore, combineReducers, applyMiddleware } from 'redux';
import Thunk from 'redux-thunk';
import groupsReducer from './reducers/Groups';
import itemsReducer from './reducers/Items';

const rootReducer = combineReducers({
    groups: groupsReducer,
    items: itemsReducer
});

const store = createStore(rootReducer, applyMiddleware(Thunk));

export default store;