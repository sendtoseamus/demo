import firebase from 'firebase';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { randomBrand, randomImage, randomItemName, randomOwner, randomSize } from '../../data/dummy';
import Item from '../../models/Item';

export const LOAD_ITEMS = 'LOAD_ITEMS';
export const CREATE_ITEM = 'CREATE_ITEM';

export const loadItemsAction = (groupId) => {
    return async dispatch => {

        const db = firebase.firestore();
        const snapshot = await db.collection('items').where("groupId", "==", groupId).get();

        const items = snapshot.docs.map(doc => {
            console.log(doc.id, '=>', doc.data());

            return new Item(
                doc.id,
                doc.get('name'),
                doc.get('brand'),
                doc.get('groupId'),
                doc.get('imageUrl'),
                doc.get('owner'),
                doc.get('size')
            );
        });
        console.log(items.length + " items in array\n");

        dispatch({
            type: LOAD_ITEMS,
            items: items
        });
    };
};

export const loadItems = (groupId) => {

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loadItemsAction(groupId));
    }, [dispatch]);
};

export const createNewItemAction = (groupId) => {
    return async dispatch => {

        const data = {
            name: randomItemName(),
            brand: randomBrand(),
            groupId: groupId,
            imageUrl: randomImage(),
            owner: randomOwner(),
            size: randomSize()
        };
        console.log(data);

        const db = firebase.firestore();
        const newItem = await db.collection('items').add(data);
        console.log(newItem);

        dispatch({
            type: CREATE_ITEM,
            item: newItem
        });
    };
};

export const createNewItem = (groupId) => {

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(createNewItemAction(groupId));
    }, [dispatch]);
};
