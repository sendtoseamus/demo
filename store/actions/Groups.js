import firebase from 'firebase';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { randomGroupName, randomGroupTitle, randomImage, randomValue } from '../../data/dummy';
import Group from '../../models/Group';

export const LOAD_GROUPS = 'LOAD_GROUPS';
export const CREATE_GROUP = 'CREATE_GROUP';

export const loadGroupsAction = () => {
    return async dispatch => {

        const db = firebase.firestore();
        const snapshot = await db.collection('groups').get();

        const groups = snapshot.docs.map(doc => {
            console.log(doc.id, '=>', doc.data());

            return new Group(
                doc.id,
                doc.get('name'),
                doc.get('title'),
                doc.get('imageUrl'),
                doc.get('discussionCount'),
                doc.get('listingsCount'),
                doc.get('usersCount')
            );
        });
        console.log(groups + " groups in array\n");

        dispatch({
            type: LOAD_GROUPS,
            groups: groups
        });
    };
};

export const loadGroups = () => {

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loadGroupsAction());
    }, [dispatch]);
};

export const createNewGroupAction = () => {
    return async dispatch => {

        const data = {
            name: randomGroupName(),
            title: randomGroupTitle(),
            imageUrl: randomImage(),
            discussionCount: randomValue(2500),
            listingsCount: randomValue(2500),
            usersCount: randomValue(550)
        };
        console.log(data);

        const db = firebase.firestore();
        const newGroup = await db.collection('groups').add(data);
        console.log(newGroup);

        dispatch({
            type: CREATE_GROUP,
            group: newGroup
        });
    };
};

export const createNewGroup = () => {

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(createNewGroupAction());
    }, [dispatch]);
};
